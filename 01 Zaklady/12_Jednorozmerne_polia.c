#include <stdio.h>
#include <stdlib.h>

int main()
{
    //definicia jednorozmerneho pola

    //staticke pole
    int a[10]; //pocet prvkov v poli: [10], indexy: 0-9

    int b[]={4,5,8,7,9,10}; //pocet prvkov: 6, indexy: 0-5
    for(int i=0;i<6;i++)
        printf("%d ",b[i]);
    putchar('\n');

    int c[20]={7,8,9,4,5,6};
    for(int j=0;j<20;j++)
        printf("%d ",c[j]);
    putchar('\n');

    //dynamicke pole - pole v halde
    int *p_a;
    p_a=(int*)malloc(sizeof(int)*10); //10 poloziek velkosti int - preto *10
    for(int k=0;k<10;k++) p_a[k]=k;
    for(int k=0;k<10;k++) printf("%d ",p_a[k]);
    putchar('\n');

    //iny sposob
    for(int l=0;l<10;l++) *(p_a+l)=l;
    for(int l=0;l<10;l++) printf("%d ",*(p_a+l));
    putchar('\n');

    printf("Zadaj 10 cisel: \n");
    for(int m=0;m<10;m++) scanf("%d",&p_a[m]);
    //for(int m=0;m<10;m++) scanf("%d",p_a+m);
    for(int m=0;m<10;m++) printf("%d ",*(p_a+m));

    free((void*)p_a); p_a=NULL;

    return 0;
}
