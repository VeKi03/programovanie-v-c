#include <stdio.h>
#include <stdlib.h>

void vypis(int[],int); //prvy parameter je int x[] alebo int* = pointer na pole ; druhy parameter je pocet prvkov v poli
//void vypis(int*,int)

//zadefinovanie triedenia bubblesort
void bubblesort(int[],int); //prvy parameter = triedene pole, druhy parameter = pocet prvkov v poli

//funkcia, ktorej parametre su volane odkazom
void vymen(int*,int*);

int main()
{
    srand((unsigned)time(NULL)); //inicializovanie generatora nahodnych cisel
    int *q; //dynamicke pole pouzitie pointera
    q=(int*)malloc(sizeof(int)*10);
    for(int i=0;i<10;i++)q[i]=rand()%10; //naplnenie pola nahodnymi cislami od 0 do 9
    vypis(q,10); //vypisanie pola - pouzitie funkcie
    bubblesort(q,10);
    vypis(q,10);
    free((void*)q);q=NULL; //uvolnenie pola
    return 0;
}

void vypis(int x[],int n)
{
    int i;
    for(i=0;i<n;i++)
        printf("%d ",x[i]);
    printf("\n");
}

void bubblesort(int x[],int n)
{
    for(int i=1;i<n;i++)
        for(int j=n;j>=i;j--)
            if(x[j-1]>x[j]) vymen(&x[j-1],&x[j]); //dolezite & - potrebujem odovzdat nie hodnotu ale adresu
}

void vymen(int *a, int *b) //dolezite * (kedze parametre a,b su adresy, musim sa spytat aka je ich hodnota na adrese)
{
    int c;
    c=*a;*a=*b;*b=c;
}
