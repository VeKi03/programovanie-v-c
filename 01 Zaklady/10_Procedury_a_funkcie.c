#include <stdio.h>
#include <stdlib.h>
#include <time.h> //kniznica pre datove typy a funkcie sluziace pre pracu s casom a datumom

//deklaracie funcii - nemusia byt, ak by sa funkcie nachadzali pred main()
int obsah(int,int); //deklaracia funkcie s parametrami a s navratovou hodnotou int
int nahoda(void); //deklaracia funkcie bez parametra a s navratovou hodnotou int
void hviezda(int); //deklaracia funkcie s parametrami a bez navratovej hodnoty
void trojuholnik(void); //deklaracia funkcie bez parametra a bez navratovej hodnoty

int main()
{
    //volanie funkcii a procedur

    //obsah stvorca
    int a,b,c;
    a=3; b=4;
    c=obsah(a,b);
    printf("%d * %d = %d\n",a,b,c);

    //nahodne cislo
    srand((unsigned)time(NULL));
    printf("nahoda = %d\n",nahoda());

    //hviezda
    hviezda(4);

    //trojuholnik
    trojuholnik();

    return 0;
}

/*
Obsah obdlznika S=a*b
navratova hodnota: int
parametre: int x, int y
*/
int obsah(int x, int y)
{
    int S;
    S=x*y;
    return S;
}

/*
Nahodne cislo od 0 do 100
navratova hodnota: int
bez parametrov
*/
int nahoda(void)
{
    return rand()%101; //generuje cisla od 0 do 100
}

/*
Procedura na hviezdicky
bez navratovej hodnoty
parametre: int
*/
void hviezda(int x)
{
    int i=0;
    for(i=0;i<x;i++) putchar('*');
    putchar('\n');
    return; //nemusi
}

/*
Procedura trojuholnik
bez navratovej hodnoty
bez parametrov
*/
void trojuholnik(void)
{
    int i,j;
    for(i=0;i<5;i++)
    {
        for(j=0;j<5-i;j++) putchar(' ');
        for(j=0;j<2*i-1;j++) putchar('*');
        putchar('\n');
    }
}

