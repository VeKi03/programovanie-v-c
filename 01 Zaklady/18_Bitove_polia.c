#include <stdio.h>
#include <stdlib.h>

//bitove polia - umoznuju zmensit rezervovanu pamat pre danu premennu

typedef struct
{
    int d:6; //1-31dni - v skutocnosti staci 6bitov
    int m:5; //1-12mesiacov - staci 5bitov
    int r:8; //1970-2070 100rokov - staci 8bitov
    //dokopy stacia 3bajty z 12, ktore ma 3*int=3*4=12 (v tomto priklade)
}DATUM;

int main()
{
    int x,y,z;
    DATUM datum;
    datum.d=12;
    datum.m=12;
    datum.r=36;
    printf("%d.%d.%d\n",datum.d,datum.m,1970+datum.r);

    //nacitavanie do bitoveho pola

    //nemozeme pouzit scanf() priamo, pretoze identifikatory nereprezentuju konkretne adresy a nemozme sa na ne odvolavat
    //scanf("%d %d %d",&datum.d,&datum.m,&datum.r); - !neda sa pouzit ich adresa, kvoli posunu bitov
    //spravny funkcny postup:
    scanf("%d %d %d",&x,&y,&z);
    datum.d=x;datum.m=y;datum.r=z;
    printf("%d.%d.%d\n",datum.d,datum.m,1970+datum.r);

    return 0;
}
