#include <stdio.h>
#include <stdlib.h>

//Typ struktura

//globalna definicia struktury
struct clovek
{
    char meno[15];
    int vek;
};

/*
struct clovek
{
    char meno[15];
    int vek;
}a; //premenna struktury definovana priamo pri definici struktury - nahrada "struct clovek a;"
*/


/*
struct //bez identifikatora: "clovek" - dalej sa nemozme na tuto strukturu v programe odvolavat - vyhoda: ak strukturu nechceme dat k dispozicii inym programatorom
{
    char meno[15];
    int vek;
}a;
*/

//premenna definovana ako typ struktura
//v strukture sa v pamati nachadzaju jednotlive polozky za sebou, napr. v tomto priklade najprv 15bajtov mena a potom 4bajty veku ako sucet
typedef struct clovek2 //nazov struktury: clovek2
{
    char meno[15];
    int vek;
    //struct clovek *nasl; //struktura ukazuje samu na seba - spajany zoznam/zasobnik/rad
}CLOVEK2; //nazov typu struktura clovek2


//Typ unia
//velkost tohto typu sa bude rovnat velkosti najvacsej zlozky, v tomto pripade float - polozky su prekryte na sebe poukladane - pristup je rovnaky ako pri typu struktura
//pri praci s registrami
typedef union cisla
{
    char x;
    int y;
    float z;
}CISLA;

//Typ vymenovanim prvkov
//{konstanty, ktore sa daju priradovat premennej typu TYZDEN, ktora je podmnozinou typu int}
//pon=0, uto=1 atd. - mozme to vsak aj zmenit ako je v priklade (bitove kodovanie)
typedef enum{pon=1,uto=2,str=4,stv=8,pia=16,sob=32,ned=64}TYZDEN;


int main()
{
    //pouzitie globalnej struktury
    struct clovek a; //v jednej premmenej sa skryvaju 2 udaje, ktore su zabalene do struktury
    a.vek=15;
    strcpy(a.meno,"Aladar");
    printf("%s %d\n",a.meno,a.vek);

    CLOVEK2 b; //staci len identifikator - typedef
    b.vek=20;
    strcpy(b.meno,"Matej");
    printf("%s %d\n",b.meno,b.vek);

    //pointer na strukturu
    CLOVEK2 *c;
    c=(CLOVEK2*)malloc(sizeof(CLOVEK2));
    (*c).vek=30;
    strcpy((*c).meno,"Daniel");
    printf("%s %d\n",(*c).meno,(*c).vek);
    //iny zapis dynamickej premennej (*d) pomocou sipky
    //ak mam dynamicke premenne alebo teda pointer na strukturu, tak sa k polozkam struktury dostavam cez -> sipku
    c->vek=30;
    strcpy(c->meno,"Daniel");
    printf("%s %d\n",c->meno,c->vek);
    free((void*)c);


    TYZDEN d;
    d=stv;
    printf("%d\n",d);
    return 0;
}
