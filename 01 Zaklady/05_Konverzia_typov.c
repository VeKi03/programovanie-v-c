#include <stdio.h>
#include <stdlib.h>

int main()
{
    //Konverzia typov = pretypovanie
    int a;
    float f;
    f=56; //implicitna typova konverzia

    //float sa automaticky konvertuje na typ double - doplnia sa nuly
    //char sa implicitne konvertuje na typ int

    a=(int)7.89; //explicitne pretypovanie - ja urcujem
    printf("a = %d, f = %f\n",a,f);

    return 0;
}

