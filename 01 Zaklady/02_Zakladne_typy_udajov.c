//hlavickove subory (kniznice)
#include <stdio.h> //pre vstup a vystup
#include <stdlib.h>

//datove typy           //format vypisu

//znaky
char a;                 //%c
//cele cisla
char b;                 //%d
unsigned char c;        //%u
signed char d;          //%i
short int e;            //%d
long int f;             //%ld
int g;                  //%d
unsigned short int h;   //%u
unsigned long int i;    //%lu
unsigned int j;         //%u
signed short int k;     //%i
signed long int l;      //%li
signed int m;           //%i
//s desatinnou ciarkou
float n;                //%f
double o;               //%f
long double p;          //%Lf

/*
    vypis velkosti jednotlivych typov udajov

    printf - vypis na obrazovku
    sizeof - zisti rozsah datoveho typu v bytoch
    \n - novy riadok
    %d - ur�uje form�t v�pisu, cel� dekadick� ��slo
*/
int main()
{
    printf("Velkosti jednoduchych datovych typov:\n\n");
    printf("Velkost char = %d B\n",sizeof(char));
//  printf("Velkost char = %d B\n",sizeof(a)); // a - premenna
    printf("Velkost short int = %d B\n",sizeof(short int));
    printf("Velkost long int = %d B\n",sizeof(long int));
    printf("Velkost int = %d B\n",sizeof(int));
    printf("Velkost unsigned short int = %d B\n",sizeof(unsigned short int));
    printf("Velkost unsigned long int = %d B\n",sizeof(unsigned long int));
    printf("Velkost unsigned int = %d B\n",sizeof(unsigned int));
    printf("Velkost signed short int = %d B\n",sizeof(signed short int));
    printf("Velkost signed long int = %d B\n",sizeof(signed long int));
    printf("Velkost signed int = %d B\n",sizeof(signed int));
    printf("Velkost float = %d B\n",sizeof(float));
    printf("Velkost double = %d B\n",sizeof(double));
    printf("Velkost long double = %d B\n",sizeof(long double));
    return 0;
}
