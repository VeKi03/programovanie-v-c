#include <stdio.h>
#include <stdlib.h>

//pointer - specialny typ udajov - praca s haldou
int main()
{
    //definicia pointera
    int *p_a,a; //premenna p_a je pointer na premennu "a" typu int

    //inicializacia pointera
    p_a=NULL; //neukazuje nikde = nulovy
    p_a=&a; //ukazuje na konkretnu premennu
    //p_a=1; //chybne priradenie, pointer je adresa!
    *p_a=1; //operator hviezdicka * - na miesto kam ukazuje premenna pointer p_a vloz hodnotu 1

    printf("%d\n",a);
    printf("%p\n",p_a); //%p - vypis adresy na ktorej je premenna "a", vypise v sestnastkovej sustave
    printf("%d\n",*p_a); //vypis hodnoty z premennej "a" pomocou pointera

    printf("Zadaj cislo: \n");
    scanf("%d",&a);
    printf("%d\n",*p_a);
    //scanf("%d",p_a); //nemusim pisat & - pointer je sam o sebe adresou

    //spristupnenie haldy pomocou pointera
    int *p_b;
    //alokacia miesta pre premennu v halde - prikaz malloc + sizeof (ake velke miesto chcem alokovat)
    //vznik premennej v halde
    p_b=(int*)malloc(sizeof(int)); //funkcia malloc vracia pointer na typ void - pretypujem na int (int*)
    //priradenie hodnoty
    *p_b=4;
    printf("%d\n",*p_b);

    /*
    ak je premenna dynamicka - potrebne je jej odstranenie - prikaz free

    free -> odstranuje premenne z pointerov na typ void - pretypujem z int na void
    p_b=NULL; -> miesto kam ukazuje pointer je volne na ukladanie (alokovanie) pre dalsie premenne - moze tam byt vsak este adresa - preto p_b=NULL
    */
    free((void*)p_b); p_b=NULL;

    return 0;
}

