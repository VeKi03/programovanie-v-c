#include <stdio.h>
#include <stdlib.h>

/* 4 definicie dvojrozmerneho pola:
    1. pointer na pole
    2. staticke pole
    3. pole pointerov
    4. pointer na pointer
*/


int main()
{
    //Staticke dvojrozmerne pole
    printf("Staticke dvojrozmerne pole\n");
    int m[10][10];
    srand((unsigned)time(NULL));
    int i,j;
    //naplnenie pola nahodnymi cislami
    for(i=0;i<10;i++)
        for(j=0;j<10;j++)
            m[i][j]=rand()%10;
    //vypis
    for(i=0;i<10;i++)
    {
        for(j=0;j<10;j++)
            printf("%d ",m[i][j]);
        printf("\n");
    }

    printf("\nDynamicke pole - pointer na pointer\n");
    //Dynamicke pole - pointer na pointer
    int **n; //** = pointer na pointer
    n=(int**)malloc(sizeof(int*)*10); //vytvori pole pointerov
    for(i=0;i<10;i++) n[i]=(int*)malloc(sizeof(int)*10); //v cykle na kazdy pointer v poli pointerov vytvori nove pole typu int
    //naplnenie pola nahodnymi cislami
    for(i=0;i<10;i++)
        for(j=0;j<10;j++)
            n[i][j]=rand()%10;
    //vypis
    for(i=0;i<10;i++)
    {
        for(j=0;j<10;j++)
            printf("%d ",n[i][j]);
        printf("\n");
    }

    for(i=0;i<10;i++) free((void*)n[i]); //uvolnenie jednorozmernych poli n[i]
    free((void*)n);n=NULL; //uvolnenie celeho pola pointerov

    //Pole vymenovanim prvkov
    printf("\nPole vymenovanim prvkov\n");
    int o[][3]={{5,6,4},{4,4,8}};// pole 2x3
    for(i=0;i<2;i++)
    {
        for(j=0;j<3;j++)
            printf("%d ",o[i][j]);
        printf("\n");
    }
    return 0;
}
