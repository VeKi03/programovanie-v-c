#include <stdio.h>
#include <stdlib.h>

int main()
{
    //Binarne vetvenie = prva riadiaca struktura = lekcia

    //Porovnanie dvoch cisel
    int a,b;
    printf("Zadaj prve cislo.\na = "); scanf("%d",&a);
    printf("Zadaj druhe cislo.\nb = "); scanf("%d",&b);

    if(a<b) printf("\na < b\n"); //za if nasleduje iba jeden prikaz
    else if(a>b) printf("\na > b\n");
         else printf("\na = b\n");

    //Sekvencia zlozenych prikazov
    printf("\nZlozene prikazy\n");
    printf("\nZadaj prve cislo.\na = "); scanf("%d",&a);
    printf("Zadaj druhe cislo.\nb = "); scanf("%d",&b);
    //viac prikazov --> {}
    if(a < b)
    {
        printf("\na < b\n");
        printf("prikaz za if\n");
    }
    else if(a > b)
    {
        printf("\na > b\n");
        printf("prikaz za prvym else\n");
    }
        else
        {
            printf("\na = b\n");
            printf("prikaz za druhym else\n");
        }

    return 0;
}
