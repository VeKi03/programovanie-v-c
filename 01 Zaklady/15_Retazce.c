#include <stdio.h>
#include <stdlib.h>
#include <string.h> //strcpy

//retazec = pole znakov
//strlen() - dlzka retazca
//strcmp() - porovnavanie dvoch retazcov
//strstr() - vyhladavanie podretazca v retazci
//strchr() - hladanie vyskytu znaku v retazci

int main()
{
    //definovanie retazcov

    //Staticky retazec - premenna a vznikla v zasobniku
    //priame priradenie
    char a[15]="Ahoj!"; //ako pole typu char [alokovany pocet znakov v retazci], na konci retazca sa nachadza symbol \0, ak nie je pokracuje az na najblizsiu hodnotu \0 v retazci
    char b[15];
    //vypis retazca - %s
    printf("%s\n",a);
    printf("Dlzka retazca = %d\n",strlen(a));
    //pomocou prikazu strcpy kniznice string.h
    strcpy(b,"Hello!");  //umoznuje priradovat do premennej retazcove hodnoty
    printf("\n%s\n",b);
    printf("Dlzka retazca = %d\n",strlen(b));

    //Dynamicky retazec - cez pointre - premenna b vznikla v halde
    //- nie je mozne priamo priradit hodnotu retazca - len cez strcpy
    char *c;
    c=(char*)malloc(sizeof(char)*15);
    strcpy(c,"Hi!");
    printf("\n%s\n",c);
    printf("Dlzka retazca = %d\n",strlen(c));
    free((void*)c);c=NULL;

    a[4]='?'; //zmena znaku v retazci na zadanej pozicii

    //Retazec ako pole znakov
    //vypisanie retazca pomocou cyklu
    printf("\nRetazec ako pole znakov\n");
    for(int i=0;i<strlen(a);i++)
        putchar(a[i]);
    printf("\n");

    //Nahodny retazec
    printf("\nNahodny retazec\n");
    srand((unsigned)time(NULL));
    for(int i=0;i<14;i++)
        b[i]=(rand()%('Z'-'A')) + 'A'; //nahodne znaky (znakov je presne tolko kolko je rozdiel medzi velkym pismenom Z a A + nahodna hodnota 'A' (zaruka velkych pismen)
    b[14]='\0'; //na miesto posledneho znaku zapiseme symbol \0 - koniec retazca
    printf("%s\n",b);

    return 0;
}
