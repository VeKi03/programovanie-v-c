#include <stdio.h>
#include <stdlib.h>

int main()
{
    //lokalne premenne
    int a=2,b=3;
    float f=37.89564;
    int c=91;

    //vypis
    printf("Sposoby vstupu a vystupu z konzoly.\n\a"); // \a - pipnutie speakera(alert)
    printf("a = %d \nb = %d\n",a,b);

    //desatinne
    printf("f = %f\n",f); //%f - s desatinnou ciarkou
    printf("f = %.2f\n",f); //%.2f - zaokruhlenie na 2 desatinne miesta
    printf("f = %e\n",f); //%e - v logaritmickom tvare

    //konvertovanie
    printf("%d\n",c);
    printf("%c\n",c); //%c - znak z ASCII
    printf("%o\n",c); //%o - v osmickovej sustave
    printf("%x\n",c); //%x - v sestnastkovej sustave s malymi pismenami
    printf("%X\n",c); //%X - v sestnastkovej sustave s velkymi pismenami

    //nacitavanie a nasledny vypis
    printf("Zadaj cislo: ");
    scanf("%d",&c); //potrebny &(ampersand)
    printf("Tvoje cislo: %d\n",c);

    fflush(stdin); //zajisti vyprazdnenie vyrovnavacej pamati do souboru

    printf("Zadaj znak: ");
    scanf("%c",&c);
    printf("Tvoj znak ma hodnotu: %d\n",c); //vypise cislo z ASCII

    char d;
    printf("Zadaj cele cislo, realne cislo a znak:\n");
    scanf("%d %f %c",&a,&f,&d); //viac premennych - potrebne medzery
    printf("Zadal si:\n%d\n%f\n%c\n",a,f,d);

    fflush(stdin);

    //specialny vstup a vystup - nacitavanie z klavesnice
    printf("Zadaj vstup z klavesnice: ");
    d=getchar(); //nacitanie
    printf("Zadal si: ");
    putchar(d); //vypis na obrazovku

    return 0;
}

