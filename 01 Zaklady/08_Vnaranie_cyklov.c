#include <stdio.h>
#include <stdlib.h>

int main()
{
    //nakreslenie stvorca n*n hviezdiciek

    int n=10;

    //cyklus for+for
    printf("for+for\n");
    int i,j;
    for(j=0;j<n;j++)
    {
        for(i=0;i<n;i++) putchar('*');
        putchar('\n');
    }

    printf("\n------------------------------------------\n\n");

    //cyklus while + for
    printf("while+for\n");
    j=0;
    while(j<n)
    {
        for(i=0;i<n;i++) putchar('*');
        putchar('\n');
        j++;
    }

    printf("\n------------------------------------------\n\n");

    //cyklus while+while
    printf("while+while\n");
    j=0;
    while(j<n)
    {
        i=0;
        while(i<n)
        {
            putchar('*');
            i++;
        }
        putchar('\n');
        j++;
    }

    printf("\n------------------------------------------\n\n");

    //cyklus do-while+do-while
    printf("do-while+do-while\n");
    j=0;
    do
    {
        i=0;
        do
        {
            putchar('*');
            i++;
        } while(i<n);
        putchar('\n');
        j++;
    }while(j<n);

    printf("\n------------------------------------------\n\n");

    //cyklus do-while+while
    printf("do-while+while\n");
    j=0;
    do
    {
        i=0;
        while(i<n)
        {
            putchar('*');
            i++;
        }
        putchar('\n');
        j++;
    }while(j<n);

    return 0;
}

