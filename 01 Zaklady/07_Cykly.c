#include <stdio.h>
#include <stdlib.h>

//iteracia = cyklus

int main()
{
    //nakreslenie n hviezdiciek na obrazovke
    int n=10;
    int c=10;
    //cyklus For - s podmienkou opakovani
    for(int i=0;i<n;i++,c--)
    {
            putchar('*');
            printf("%d",c);

    }
    putchar('\n');

    /*
    int i;
    for(i=0;i<n;i++)
    */

    /*
    int i=0;
    for(i<n;i++)
    */

    for(int y=0;y<n;y+=2)
    putchar('*');
    putchar('\n');

    //cyklus While - s podmienkou nazaciatku
    int j=0;
    while(j<n)
    {
        putchar('*');
        j++;
    }
     putchar('\n');

    //cyklus Do-while - s podmienkou nakonci
    int k=0;
    do
    {
        putchar('*');
        k++;
    }while(k<n);
    putchar('\n');

    return 0;
}
