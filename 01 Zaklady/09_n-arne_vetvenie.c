#include <stdio.h>
#include <stdlib.h>

int main()
{
    //Pre znaky
    int c;
    printf("Zadaj znak: \n");
    c=getchar();
    switch(c)
    {
            case 'a':printf("Isiel som prvou vetvou.\n"); break;
            case 'b':printf("Isiel som druhou vetvou.\n"); break;
            case 'c':printf("Isiel som tretou vetvou.\n"); break;
            default:printf("Preskocil som to.\n"); break;
    }

    //Pre cele cisla
    int i;
    printf("\nZadaj cislo: \n");
    scanf("%d",&i);
    switch(i)
    {
            case 1:printf("Isiel som prvou vetvou.\n"); break;
            case 2:printf("Isiel som druhou vetvou.\n"); break;
            case 3:printf("Isiel som tretou vetvou.\n"); break;
            default:printf("Preskocil som to.\n"); break;
    }

    return 0;
}


