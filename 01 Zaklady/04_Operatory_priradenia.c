#include <stdio.h>
#include <stdlib.h>

int main()
{
    //sposoby priradenia hodnoty premennej
    printf("Sposoby priradenia hodnoty premennej.\n\n");
    int i=3; //priamo pri jej definicii
    printf("i = %d\n",i);
    int j,k,l;
    j=5; //mimo definicie
    printf("j = %d\n",j);
    l=k=i=4; //naraz do premennych priaradime rovnaku hodnotu
    printf("l = %d, k = %d, i = %d\n",l,k,i);

    //matematicke operacie
    printf("\nMatematicke operacie.\n\n");
    k+=4; //k=k+4;
    l-=1; //l=l-1;
    j*=3; //j=j*3;
    i/=2; //i=i/2;
    printf("Scitanie k+4: %d\n",k);
    printf("Odcitanie l-1: %d\n",l);
    printf("Nasobenie j*3: %d\n",j);
    printf("Delenie i/2: %d\n",i);

    //operatory inkrementacie a dekrementacie
    printf("\nOperatory inkrementacie a dekrementacie.\n");
    i=j=10;
    printf("\ni = %d, j = %d\n",i,j);
    i++; //++i; i=i+1; //++ = inkrement
    j--; //--i; i=j-1; //-- = dekrement
    printf("i++ = %d, j-- = %d\n",i,j);

    printf("\nPrefixovy zapis inkrementacie k=++i\n");
    printf("i = %d, k = %d\n",i,k);
    k=++i; //i++; k=i;
    printf("i = %d, k = %d\n",i,k);

    printf("\nSufixovy zapis inkrementacie l=i++\n");
    printf("i = %d, l = %d\n",i,l);
    l=i++; //l=i; i++;
    printf("i = %d, l = %d\n",i,l);

    //Ternarny operator priradenia
    printf("\nTernarny operator priradenia napr. k=(i>j)?5:6;\n");
    i=10;
    j=15;
    k=(i>j)?5:6; //ak je podmienka v zatvorke splnena tak do k sa priradi hodnota za otaznikom, ak nie je splnena tak hodnota za dvojbodkou
    printf("i = %d, j = %d, k = %d\n",i,j,k);

    i=15;
    j=10;
    k=(i>j)?5:6;
    printf("i = %d, j = %d, k = %d\n",i,j,k);

    return 0;
}

